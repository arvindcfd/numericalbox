#include<iostream>
#include<vector>
#include<cmath>
#include <algorithm>
#include "printing.h"
#include "arrayops.h"
#include "linearsolvers.h"

int main()
{
    //arrayops ob;
    linearsolvers oj;
    /*
    std::vector<double> k = ob.array1dgen(4,1.0);
    std::vector<double> l = ob.array1dgen(4,2.0);
    std::vector<double> d = mod(diff(k,l));
    */

    std::vector< std::vector<double> > eqn = {{10,-2,-1,-1},{-2,10,-1,-1},{-1,-1,10,-2},{-1,-1,-2,10}};
    std::vector<double> b = {3,15,27,-9};

    /*
    print_array2d(eqn,4,4);
    std::cout << "\n";
    print_array1d(b);
    */
    std::vector<double> sol = oj.gauss_seidel(eqn,b,0);

   // std::vector<double> modu = mod(d);
    //d = l;
    //std::cout << "mod" << mod << "\n";

    std::cout<< "The final solution is:" <"\n";
    print_array1d(sol);

    /*
    double e = minval(d);
    std::cout << "max is" << e << "\n";
    print_array1d(d);
    */
    return 0;
}

/*
int m = 21;
int n = 21;

float delx,dely;
std::vector< std::vector<double> > phi_init,phi_ana,phi_new;
std::vector<double> x,y;

arrayops ob;

x = ob.array1dgen(n,2.0);
y = ob.array1dgen(m,2.0);

delx = 1.0/(m-1);
dely = 1.0/(n-1);
std::cout << "delx" << delx <<"\t" << "dely" << dely <<"\n";

phi_init = ob.array2dgen(m,n,0);

std::cout << "phi_init" << "\n";

print_array2d(phi_init,m,n);

// Analytical Solution

phi_ana = ob.array2dgen(m,n,0);

 for (int i=0;i<m;i++)
 {
     x[i] = i*delx;
       for (int j=0;j<n;j++)
       {
           y[j] = j*dely;
           phi_ana[i][j] = 500*exp(-50*(pow((1.0-x[i]),2) + pow(y[j],2))) + 100*x[i]*(1.0-y[j]);
       }
 }

print_array2d(phi_ana,m,n);

print_array1d(x);

//array2d_invert(phi_ana);

// Init boundary values
*/
