/* Library to just print arrays out .... Just to make life easier from for loops */

#ifndef PRINTING_H_INCLUDED
#define PRINTING_H_INCLUDED
#include <iostream>
template <class T>
void print_array2d (std::vector< std::vector<T> >& x, const int& m, const int& n)
{
    //std::cout << "The 2d array is:" << "\n";
//    int asize = x.size();
    for (int i=0;i<m;i++)
    {
        for (int j=0;j<n;j++)
        {
            std::cout << x[i][j] << "\t";  //printf("%f", phi_init[i][j]);
        }
        std::cout << "\n";
    }
    //std::cout << "size is" << asize << "\n";
}

template <class T>
void print_array1d (std::vector<T>& x)
{
    const int asize = x.size();
  //std::cout << "The 1d array is:" << "\n";
    for (int i=0;i<asize;i++)
        {
            std::cout << x[i] << "\t";  //printf("%f", phi_init[i][j]);
        }
        std::cout << "\n";
}

#endif // PRINTING_H_INCLUDED
